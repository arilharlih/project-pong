﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallControl : MonoBehaviour
{
	// RigidBody Bola
	private Rigidbody2D rigidBody2D;

	// Besar gaya awal
	public float xInitialForce;
	public float yInitialForce;

	// Titik asal lintasan bola saat ini
    private Vector2 trajectoryOrigin;
    
    void OnCollisionExit2D(Collision2D collision) {
    	trajectoryOrigin = transform.position;
    }
    public Vector2 TrajectoryOrigin {
    	get {return trajectoryOrigin;}
    }

    // Start is called before the first frame update
    void Start()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
        trajectoryOrigin = transform.position;
        // Mulai game
        RestartGame();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ResetBall() {
    	// Reset Posisi
    	transform.position = Vector2.zero;
    	// Reset kecepatan
    	rigidBody2D.velocity = Vector2.zero;
    }

    void PushBall() {
    	// Tentukan nilai acak dari 0 (Inklusif) dan 2 (eksklusif)
    	float randomDirection = Random.Range(0,2);

    	// Jika nilai dibawah 1 bergerak kekiri, dan sebaliknya
    	if (randomDirection < 1.0f) {
    		// Daya untuk menggerkkan bola
    		rigidBody2D.AddForce(new Vector2(-xInitialForce, yInitialForce));
    	} else {
    		rigidBody2D.AddForce(new Vector2(xInitialForce, yInitialForce));
    	}
    }

    void RestartGame() {
    	// Kembali semula
    	ResetBall();

    	// Setelah 2 detik, beri gaya
    	Invoke("PushBall", 2);
    }

}
